from sqlite3 import Cursor
from ..models import cursor, database
import ipdb


class CallFee:
    
    def __init__(self, origin, destiny, fee, duration, error):
        self.origin = origin
        self.destiny = destiny
        self.fee = fee
        self.duration = int(duration)
        
        self.error = False

        
        
    @classmethod
    @database.save_changes    
    def insert_database_call_fee(cls, cursor: Cursor, origin, destiny, fee):
        cursor.execute("""
            INSERT INTO call_fee
                (origin, destiny, fee)
            VALUES
                (?, ?, ?)
            """, (origin, destiny, fee)
        )


    @classmethod
    def show_fee_by_origin_destiny(cls, cursor: Cursor, origin: str, destiny: str, duration: int, error: bool):

        cursor.execute(f"""
            SELECT fee FROM call_fee
            WHERE origin = ? AND destiny = ?
            """, (origin, destiny)
        )
                
        found_fee = cursor.fetchone()

        if type(found_fee) != tuple:
            error = True
            found_fee = 'error'
            found_fee = cls(origin, destiny,  found_fee, duration, error)
            return found_fee
            
        
        else:
            error = False
            found_fee = found_fee[0]
            found_fee = cls(origin, destiny, found_fee, duration, error)
            return found_fee


    def calculate_fees_by_duration(self, cursor: Cursor, free_minutes: int):
        
        if self.origin == self.destiny:
            return 'error'            
                    
        if free_minutes == 'error':
            return 'error'
        
        elif self.error == True:
            return 'error'
        
        elif self.duration <= 0: 
            return 'error'

        elif type(self.fee) != float:
            return 'error'

        elif self.duration < free_minutes:
            full_fee = self.fee * self.duration
            discount_fee = 'sem custo'
                                            
        
        else:

            full_fee = self.fee * self.duration
            
            exceeded_minutes = (self.fee * (self.duration - free_minutes))
                        
            discount_fee = round(exceeded_minutes * 1.1, 2)

            if type(full_fee) != float or type(discount_fee) != float and type(discount_fee) != str:
                
                return 'error'
            
        return (full_fee, discount_fee)


