from sqlite3 import Cursor
from ..models import cursor, database
import ipdb


class PlanTypes:
    
    def __init__(self, plan_name):

        self.plan_name = plan_name


    @classmethod
    @database.save_changes    
    def insert_database_plan(cls, cursor: Cursor, plan_name, free_minutes, increment_exceeded_minute):
        cursor.execute("""
            INSERT INTO plan_minutes
                (plan_name, free_minutes, increment_exceeded_minute)
            VALUES
                (?, ?, ?)
            """, (plan_name, free_minutes, increment_exceeded_minute)
        )


    @classmethod
    def show_free_minutes_by_plan(cls, cursor: Cursor, plan_name: str):
            
        cursor.execute(f"""
            SELECT free_minutes FROM plan_minutes
            WHERE plan_name= ?
            """, (plan_name, )
        )
        
        free_minutes = cursor.fetchone()
        
        if type(free_minutes) != tuple:
            return 'error'
        
        else:
            free_minutes = free_minutes[0]
            return free_minutes


