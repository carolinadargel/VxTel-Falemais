from flask import Flask, Blueprint, request, render_template, redirect, json
from json import dumps

from app.models import cursor, Database
from app.models.callFee import cursor, CallFee
from app.models.planTypes import cursor, PlanTypes


site = Blueprint('site', __name__, template_folder='templates')

@site.route('/', methods=['GET', 'POST'])

def home():
    
    if request.method == 'GET':
        return render_template('/base.html')

    if request.method == 'POST':
        
        origin = request.form.get('origin')
        destiny = request.form.get('destiny')
        duration = request.form.get('duration')
        plan_name = request.form.get('plan name')
        error = False
        
        found_fee = CallFee.show_fee_by_origin_destiny(cursor, origin, destiny, duration, error)
        free_minutes = PlanTypes.show_free_minutes_by_plan(cursor, plan_name)

        total_fees = found_fee.calculate_fees_by_duration(cursor, free_minutes)
       
               
        if total_fees[0] == 'error' or total_fees == 'error':
            return render_template('/invalid_data.html')
    
            
        else:
            full_fee = round(total_fees[0],2)
            full_fee = 'R$ ' + str(full_fee)
            
            if int(duration) == int(free_minutes):
                discount_fee = 'sem custo'
                
            elif type(total_fees[1]) == str:
                discount_fee = total_fees[1]

            else:
                discount_fee = round(total_fees[1],2)
                discount_fee = 'R$ ' + str(discount_fee)
                        
    return render_template('/calculate_fees.html', total_fee=(full_fee, discount_fee, plan_name, origin, destiny, int(duration)))
