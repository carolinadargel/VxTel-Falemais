from flask import Flask
from app.views import site
from os import environ


def create_app():
    app = Flask(__name__)

    app.register_blueprint(site)

    return app

