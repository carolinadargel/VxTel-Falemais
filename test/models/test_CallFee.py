import unittest
from app.models.callFee import CallFee
from sqlite3 import connect
from test.models import Database

class TestCallFee(unittest.TestCase):

    def setUp(self):
        conn = connect("TesteFaleMais.db", check_same_thread=False)
        cursor = conn.cursor()
        Database(cursor, conn)
        self.cursor = cursor
    
    
    def tearDown(self):
        pass    
    
    
    def test_show_fee_by_origin_destiny(self):
    
        origin = ['011', '016', '017', '018']
        destiny = ['011', '016', '017', '018']
        duration = [20, 40, 50, 200, 1000, 0]
        error = [False, True]
        
        result1 = CallFee.show_fee_by_origin_destiny(self.cursor, origin[0], destiny[1], duration[0], error[0])
        result2 = CallFee.show_fee_by_origin_destiny(self.cursor, origin[0], destiny[2], duration[1], error[1])
        result3 = CallFee.show_fee_by_origin_destiny(self.cursor, origin[0], destiny[3], duration[2], error[0])
        result4 = CallFee.show_fee_by_origin_destiny(self.cursor, origin[1], destiny[0], duration[3], error[0])
        result5 = CallFee.show_fee_by_origin_destiny(self.cursor, origin[2], destiny[0], duration[4], error[0])
        result6 = CallFee.show_fee_by_origin_destiny(self.cursor, origin[3], destiny[0], duration[5], error[0])
        
        result1 = result1.calculate_fees_by_duration(self.cursor, duration[0])
        result2 = result2.calculate_fees_by_duration(self.cursor, duration[1])
        result3 = result3.calculate_fees_by_duration(self.cursor, duration[2])
        result4 = result4.calculate_fees_by_duration(self.cursor, duration[3])
        result5 = result5.calculate_fees_by_duration(self.cursor, duration[4])
        result6 = result6.calculate_fees_by_duration(self.cursor, duration[5])



        self.assertEqual(result1, (38.0, 0.0))
        self.assertEqual(result2, (68.0, 0.0))
        self.assertEqual(result3,(45.0, 0.0))
        self.assertEqual(result4, (580.0, 0.0))
        self.assertEqual(result5, (2700.0, 0.0))
        self.assertEqual(result6, 'error')


    
    