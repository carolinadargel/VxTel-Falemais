import unittest
from app.models.planTypes import PlanTypes
from sqlite3 import connect
from test.models import Database

class TestPlanTypes(unittest.TestCase):

   def setUp(self):
      conn = connect("TesteFaleMais.db", check_same_thread=False)
      cursor = conn.cursor()
      Database(cursor, conn)
      self.cursor = cursor
    
    
   def tearDown(self):
      pass       
     
     
   def test_show_free_minutes_by_plan(self):

      plan = ['FaleMais 30', 'FaleMais 60', 'FaleMais 120']
        
      result1 = PlanTypes.show_free_minutes_by_plan(self.cursor, plan[0])
      result2 = PlanTypes.show_free_minutes_by_plan(self.cursor, plan[1])
      result3 = PlanTypes.show_free_minutes_by_plan(self.cursor, plan[2])
        
      self.assertEqual(result1, 30)
      self.assertEqual(result2, 60)
      self.assertEqual(result3, 120)