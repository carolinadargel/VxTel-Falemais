from sqlite3 import connect, Connection, Cursor

class Database:
    def __init__(self, connect: Connection, cursor: Cursor):
                
        self.cursor = cursor  
        self.conn = conn
          
        self.create_table_call_fee()
        self.create_table_plan_minutes()

        
             
    def create_table_call_fee(self):
        self.cursor.execute("""
            CREATE TABLE IF NOT EXISTS call_fee (
            id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            origin VACHAR(50) NOT NULL,
            destiny VARCHAR(50) NOT NULL,
            fee REAL NOT NULL
            
            )
        """)

        self.conn.commit()
        
        
    def create_table_plan_minutes(self):
        self.cursor.execute("""
            CREATE TABLE IF NOT EXISTS plan_minutes (
                id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                plan_name VACHAR(50) NOT NULL,
                free_minutes INTEGER NOT NULL,
                increment_exceeded_minute INTEGER NOT NULL
                )
            """)

        
        self.conn.commit()
        
      
    def populate_table_call_fee(self):
    
        origin = ['011', '016', '011', '017', '011', '018']
        destiny = ['016', '011', '017', '011', '018', '011']
        fees = [1.9, 2.9, 1.7, 2.7, 0.9, 1.9]
        
        count = 0 
        
        for i in fees:
        
            self.cursor.execute("""
            INSERT INTO call_fee
                (origin, destiny, fee)
                VALUES
                (?, ?, ?)
            """, (origin[count], destiny[count], fees[count])
            )
            
            count += 1
            self.conn.commit()

            
    
    def populate_table_plan_minutes(self):

        plan = [('FaleMais 30', 30, 10), ('FaleMais 60', 60, 10), ('FaleMais 120', 120, 10)]

        for i in plan:

            self.cursor.execute("""
            INSERT INTO plan_minutes
                (plan_name, free_minutes, increment_exceeded_minute)
            VALUES
                (?, ?, ?)
            """, (i[0], i[1], i[2])
            )
            self.conn.commit()


    def save_changes(self, function_that_modify_db):
        def wrapper(*args, **kwargs):
            function_return = function_that_modify_db(*args, **kwargs)
            self.conn.commit()
            return function_return
        return wrapper


conn = connect("TesteFaleMais.db", check_same_thread=False)
cursor = conn.cursor()

database = Database(conn, cursor)

