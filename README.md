**Desenvolvimento de projeto para a VxTel**

*Este programa foi desenvolvido para resolver o problema do cálculo de tarifa para o cliente da VxTel, para que possa estimar o valor da ligação com e sem o uso do Plano Fale Mais.*

**Linguagens utilizadas:** Python, HTML5, CSS 

**Biblioteca de frontend:** Bootstrap

**Gerenciamento do banco de dados:** MySQL

**Microframework:** Flask & Blueprint

**Testes:** Unittest 


1. Primeiro foi criado um banco de dados contendo as tabelas de valores por distância (DDD origem e DDD destino), além da tabela contendo os planos disponíveis e o número de minutos livres para cada plano. Esse processo acontece no arquivo app/models/__init__.py, e para que os testes possam ser executados apenas com Flask Run foram criadas também funções que populam o banco de dados. 

2. Depois foram criadas duas classes, CallFee **(app/models/callFee.py)**, e PlanTypes **(app/models/planTypes.py)** cujos métodos usam SQL para fazer pesquisa neste banco de dados.

3. Por fim, em no diretório views (app/views/__init__.py) são feitas as requisições HTTP instanciando os métodos da classe CallFee, e passando os resultados por parâmetro para o HTML usando Jinja.


**TESTES**

1. Foi utilizado o modelo de teste unitário Unittest.

2. Para rodar os testes é preciso dar o comando python -m unittest.


**ARQUITETURA**

Nesta aplicação foi usado o modelo de estruturação do Flask:

*Models*: no diretório app/models estão todas as funções que criam, populam ou consultam o banco de dados.

*Views*: no diretório app/views estão as funções que fazem requisições HTTP e recebem informações dos templates em HTML.

*Templates*: no diretório app/templates estão os arquivos HTML usados para a construção da interface gráfica para o usuário, divididos em arquivos conforme a página: base, tarifa calculada, e erro de dado inserido.

*Static*: no diretório app/static estão as imagens usadas por meio da biblioteca Bootstrap.


**Sobre a interface gráfica**

Foi criada uma página principal, com foco na pesquisa de tarifa pelo usuário, onde ele pode escolher um DDD de origem, um DDD de destino, a duração de sua chamada e o Plano Fale Mais.

Caso os dados inseridos sejam inválidos (como minutos zerados ou DDDs em combinações não cadastradas no banco de dados) o sistema devolve a página de erro. Caso contrário, será exibida a página com o cálculo de sua tarifa.

Foram cadastrados no banco de dados apenas as combinações disponíveis nas instruções passadas: ligações que começam com 011 para qualquer DDD, ou de qualquer DDD para 011.

Foram criados botões de menu e busca meramente ilustrativos.


Se tiver ficado qualquer dúvida sobre a lógica ou o código empregado, entre em contato com a desenvolvedora em *carolinadargel@gmail.com*

Agradeço a oportunidade!
